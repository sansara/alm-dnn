import os.path
import pandas as pd
import seaborn
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from sklearn.metrics import roc_curve, auc


def plot_roc(model_dir, model_name, file_logits_gt):
    """
    Create plot of ROC for the chosen predictions
    """
    df = pd.read_csv(file_logits_gt)
    logits = df["Z"].to_numpy()
    gt_labels = df["GT"].to_numpy()
    fpr, tpr, _ = roc_curve(gt_labels, logits, pos_label=1)
    roc_auc = auc(fpr, tpr)

    print("AUC = %0.3f" % roc_auc)
    plt.figure()
    lw = 2
    plt.plot(
        fpr, tpr, color="darkorange", lw=lw, label="ROC curve (area = %0.2f)" % roc_auc
    )
    plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.xticks(np.arange(0, 1.1, 0.1))
    plt.yticks(np.arange(0, 1.1, 0.1))
    plt.title("Receiver operating characteristic example")
    plt.legend(loc="lower right")
    plt.grid(ls="--", lw=0.5, markevery=0.1)
    plt.savefig(os.path.join(model_dir, model_name + "_ROC_fv.pdf"))
    plt.close()
